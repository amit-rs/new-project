import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public passwordtoggle = true;
  public confirmPassword = false;
   
  passwordtoggleHandler(){
    if(this.passwordtoggle){
      this.passwordtoggle = false;
    }else{
      this.passwordtoggle = true;
    }
  }
  confirmPasswordtoggleHandler(){
    if(this.confirmPassword){
      this.confirmPassword = false;
    }else{
      this.confirmPassword = true;
    }
  }
  constructor() { }

  ngOnInit(): void {
  }

}
