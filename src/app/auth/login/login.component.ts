import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs';
import { User } from "../user.model"

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public toggle = true;



  constructor(private route: Router, private activateRoute: ActivatedRoute) {
    this.activateRoute.url.pipe(map((segments: any) => console.log(segments)));
  }

  ngOnInit(): void {
  }

  toggleFunc() {
    if (this.toggle) {
      this.toggle = false;
    } else {
      this.toggle = true;
    }
  }

  // userModel = new User("amitrs852852@gmail.com","password@#$8528")

  user = {
    email: "amitrs852852@gmail.com",
    password: "123456"
  }


  submitHandler(information: any) {

    if (information.password == this.user.password) {
      console.log(true);
      this.route.navigate(['main/home']);
    } else {
      console.log(false);
    }
  }
  // "home/abouts"


}
