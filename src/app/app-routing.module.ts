import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: "auth",
    component: AuthComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
      }
    ]
  },
  {
    path: "main", component: LayoutComponent, children: [
      {
        path: '',
        loadChildren: () => import('./layout/layout.module').then((m) => m.LayoutModule),
      }
    ]
  },
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
